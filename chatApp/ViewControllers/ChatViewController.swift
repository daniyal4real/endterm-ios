//
//  ChatViewController.swift
//  chatApp
//
//  Created by Daniyal on 18.02.2021.
//

import UIKit
import Firebase

class ChatViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var inputTextField: UITextField!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    
    private var messagesArray: [MessageEntity] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tapGesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(moveBackInputTextField))
        tableView.addGestureRecognizer(tapGesture)
        tableView.register(UINib(nibName: MessageCell.identifier, bundle: Bundle.main), forCellReuseIdentifier: MessageCell.identifier)
        tableView.separatorStyle = .none
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 120
        
        inputTextField.delegate = self
        
        sendButton.layer.cornerRadius = 4
        sendButton.layer.masksToBounds = true
        
        retrieveMessages()
    }
    

   
    @IBAction func logOutButtonPressed(_ sender: Any) {
        do {
            print("Logged out successfully")
            try Auth.auth().signOut()
            navigationController?.popToRootViewController(animated: true)
        } catch {
            print(error)
        }
    }
    
    @IBAction func sendButtonPressed(_ sender: Any) {
        sendButton.isEnabled = false
        
        let messagesDB = Database.database().reference().child("Messages")
        guard let email = Auth.auth().currentUser?.email else { return }
        guard let message = inputTextField.text else { return }
        let messagesDict: [String: String] = ["sender": email, "message": message]
        
        messagesDB.childByAutoId().setValue(messagesDict) { (error, reference) in
            if error != nil {
                print(error!)
            } else {
                self.sendButton.isEnabled = true
                self.inputTextField.text = ""
            }
        }
    }
    
    @objc func moveBackInputTextField() {
        inputTextField.endEditing(true)
    }
    
    
    func retrieveMessages() {
        let messagesDB = Database.database().reference().child("Messages")
        
        messagesDB.observe(.childAdded) { (snapshot) in
            let snapshotValue = snapshot.value as? [String: String]
            guard let sender = snapshotValue?["sender"] else { return }
            guard let message = snapshotValue?["message"] else { return }
            
            self.messagesArray.append(MessageEntity(sender: sender, message: message))
            self.tableView.reloadData()
        }
    }
}

extension ChatViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.2) {
            self.heightConstraint.constant = 50 + 300
            self.view.layoutIfNeeded()
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.2) {
            self.heightConstraint.constant = 50
            self.view.layoutIfNeeded()
        }
    }
}

extension ChatViewController: UITableViewDelegate {
    
}



extension ChatViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messagesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MessageCell.identifier, for: indexPath) as! MessageCell
        cell.senderNameLabel.text = messagesArray[indexPath.row].sender
        cell.messageLabel.text = messagesArray[indexPath.row].message
        return cell
    }
    
}

