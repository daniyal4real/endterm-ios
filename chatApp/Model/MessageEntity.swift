//
//  MessageEntity.swift
//  chatApp
//
//  Created by Daniyal on 23.02.2021.
//

import Foundation


struct MessageEntity {
    let sender: String
    let message: String
}
